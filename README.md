# Zulip Announce Feeds

Powers RSS->[announcements on clojurians.zulipchat.com](https://clojurians.zulipchat.com/#narrow/stream/150792-announce).  
Runs hourly, see [CI pipelines](https://gitlab.com/eval/zulip-announce-feeds/pipelines).

## run 

```bash
# see options and tasks
$ ./bin/ohmyclj zulip_feeds.clj -h

# latest entries on /r/Clojure
$ ./bin/ohmyclj zulip_feeds.clj r-clj
....lots of entries
Setting Up Neanderthal with REBL and Cursive

https://www.reddit.com/r/Clojure/comments/cooiyo/setting_up_neanderthal_with_rebl_and_cursive/
t3_cooiyo
# ^^^^^ future input to last-seen
#
$ ./bin/ohmyclj zulip_feeds.clj r-clj --last-seen t3_cooiyo
...
```

## dev/test

```bash
$ ./bin/ohmyclj repl zulip_feeds.clj
$ ./bin/ohmyclj test zulip_feeds.clj
```