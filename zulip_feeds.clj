#!/usr/bin/env ./bin/ohmyclj
"DEPS='https://gitlab.com/eval/inclined.git=18202f0c4aa69ca352ec5aaf5847047bc5004c8d;buran/buran=0.1.2;selmer/selmer=1.12.12;clj-http/clj-http=3.10.0;aero/aero=1.1.3'"

(ns user
  "Post feeds to Zulip"
  #:inclined{:option.config   {:desc "Config-file to use" :default "config.edn"}
             :option.profile  {:desc "Load :dev or :prod value" :default "dev"}}
  (:require
   [aero.core :as aero]
   [buran.core :refer [consume]]
   [clj-http.client :as http]
   [clojure.pprint :as pprint]
   [selmer.parser :refer [render]]))


(def ^:private http-headers
  {"User-Agent" "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:68.0) Gecko/20100101 Firefox/68.0"})


(defn- config-for-feed [config feed-id]
  (let [keys [:input :output]]
    (reduce (fn [acc k]
              (-> acc
                  (assoc k (get-in config [k :defaults]))
                  (update k merge (get-in config [k feed-id]))))
            {} keys)))


(defn- new-entries
  "Yields all entries (oldest first) that don't contain last-seen-uri.
  In case last-seen-uri is provided but is not found in one of the
  entries, then no entries are returned (as posting all entries is
  probably not what you want)."
  [entries {:keys [last-seen-uri]}]
  (let [not-last-seen?       #(-> % :uri (not= last-seen-uri))
        newest-first         (reverse (sort-by :date entries))
        last-seen-uri-found? (some #(= % last-seen-uri)
                                   (map :uri entries))]
    (if-not (or (nil? last-seen-uri)
                last-seen-uri-found?)
      '()
      (reverse (take-while not-last-seen? newest-first)))))


(defn- new-last-seen
  "Determines new value for last-seen given entries and current
  last-seen. Assumes entries ordered oldest-first."
  [entries {:keys [last-seen]}]
  (or (-> entries last :uri)
      last-seen))


(defmulti ^:private send-msg (fn [{via :via :as _sending-cfg} _msg]
                               via))

(defmethod send-msg :log [_sending-cfg msg]
  (binding [*out* *err*]
    (println msg)))


(defmethod send-msg :zulip [{:keys [auth announce-stream announce-topic] :as _sending-cfg} msg]
  (http/post "https://clojurians.zulipchat.com/api/v1/messages"
             {:basic-auth  auth
              :form-params {:type    "stream"
                            :to      announce-stream
                            :subject announce-topic
                            :content msg}}))


(defn- consumed-feed
  ""
  [url]
  (let [headers (cond
                  (re-find #"reddit" url) http-headers
                  :else {})]
    (-> url (http/get {:headers headers}) :body consume)))


(defn- handle-feed [{:keys [url template] :as _input-cfg}
                    {:keys [last-seen-uri post-msg-fn] :or {post-msg-fn (partial send-msg {:via :log})}}]
  (let [ensure-date     (fn [{:keys [published-date updated-date] :as entry}]
                          (assoc entry :date (or published-date updated-date)))
        entries         (-> url
                            consumed-feed
                            :entries
                            (->> (map ensure-date) (sort-by :date)))
        entries-to-post (new-entries entries {:last-seen-uri last-seen-uri})]
    (doseq [msg (map #(render template %) entries-to-post)]
      (post-msg-fn msg))
    (new-last-seen entries {:last-seen last-seen-uri})))


(defn- process-task [feed-id {config-file :global/config
                              profile   :global/profile
                              :keys     [last-seen]}]
  (let [{:keys [input output]} (config-for-feed (aero/read-config config-file {:profile (keyword profile)})
                                                feed-id)
        post-msg-fn           (partial send-msg output)]
    (println (handle-feed input {:last-seen-uri last-seen :post-msg-fn post-msg-fn}))))


(defn ^#:inclined{:option.last-seen {:desc "url of last seen entry"}}
  hn
  "Latest entries from HN-feed"
  [& args]
  (apply process-task :hn args))


(defn ^#:inclined{:option.last-seen {:desc "url of last seen entry"}}
  r-clj
  "New entries from /r/Clojure"
  [& args]
  (apply process-task :r-clj args))


(defn ^#:inclined{:option.last-seen {:desc "url of last seen entry"}}
  r-cljs
  "New entries from /r/Clojurescript"
  [& args]
  (apply process-task :r-cljs args))


(defn ^#:inclined{:option.last-seen {:desc "url of last seen entry"}}
  clojureverse
  "Latest entries from Clojureverse-feed"
  [& args]
  (apply process-task :clojureverse args))


(defn ^#:inclined{:option.last-seen {:desc "url of last seen entry"}}
  clojure-qa
  "New questions from ask.clojure.org"
  [& args]
  (apply process-task :clojure-qa args))


(defn ^#:inclined{:option.last-seen {:desc "url of last seen entry"}}
  stackoverflow
  "New questions at SO"
  [& args]
  (apply process-task :stackoverflow args))


(ns test
  (:require
   [clojure.test :refer [deftest is testing are]]
   [user]))


(deftest latest-entries-test
  (let [fut     #'user/new-entries
        entries '({:uri "latest"} {:uri "middle"} {:uri "oldest"})]
    (testing "providing last-seen-uri"
      (testing "not found in any of the entries"
        (is (= '()
               (fut entries {:last-seen-uri "foo"}))))

      (testing "match with one of the entries"
        (is (= '({:uri "middle"} {:uri "oldest"})
               (fut entries {:last-seen-uri "latest"})))))))
